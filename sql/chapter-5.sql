/*  CREATED BY: Andrey Levchenko
    CREATED ON: 27.12.2022
    DESCRIPTION: Данный запрос осуществляет
    выборку полей имени, фамилии, электронной
    почты из таблицы customers (клиентов).
    и демонстрирует четыре различных способа
    использования псевдонима.
 */

SELECT FirstName,
       LastName,
       Email,
       Phone CELL
FROM customers;
SELECT FirstName AS 'First Name',
       LastName  AS [Last Name],
       Email     AS EMAIL,
       Phone        CELL,
       Company      COMPANYGAVNO
FROM customers;
/* CREATED BY: Andrey Levchenko
    CREATED ON: 27.12.2022
    DESCRIPTION: Данный запрос
    осуществляет выборку полей имени,
    фамилии и электронной почты
    из таблицы customers (клиенты),
    отсортированных по фамилии
    (Last Name).
   */
SELECT FirstName as [First Name],
       LastName  as [Last Name],
       Email     as [EMAIL]
FROM customers
order by LastName ;
/* CREATED BY: Andrey Levchenko
   CREATED ON: 27.12.2022
   DESCRIPTION: Данный запрос осуществляет
   выборку полей имени, фамилии и электронной почты
   из таблицы customers (клиенты), отсортированных
   сначала по имени (по возрастанию),
   а затем по фамилии (по убыванию).
 */
select LastName  as [Last Name],
       FirstName as [First Name],
       Email     as [EMAIL]
from customers
order by FirstName ,
         LastName desc;

/*  CREATED BY: Andrey Levchenko
    CREATED ON: 27.12.2022
    DESCRIPTION: Данный запрос осуществляет выборку первых
    10 записей из таблицы customers (клиентов),
    отсортированных сначала по имени (по возрастанию),
    а затем по фамилии (по убыванию).
   */


select FirstName as [First Name],
       LastName  as [Last Name],
       Email     as [EMAIL]
from customers
order by FirstName ,
         LastName desc
limit 10;

/*Напишите запрос, чтобы узнать количество клиентов,
    фамилии ко-торых начинаются с буквы B
*/

select LastName
from customers
order by LastName ;

/* Какая компания при сортировке впорядке
    убывания появляется в верхней строке таблицы customers?
*/
select Company
from customers
order by Company desc;
-- ответ Woodstock Discos!!!
select FirstName,
       LastName,
       PostalCode
from customers
order by PostalCode;

select
    Total as [Original Amount],
    Total + 10 as [Additional Operator],
    Total - 10 as [Substraction Operator],
    Total / 10 as [Division Operator],
    Total * 10 as [Multiplication Operator],
    Total % 10 as [Modulo Operator]

from
    invoices
order by
    Total desc;
-- Практическо задание
select
    Total as [Original Amount],
    Total * 0.15+Total as [GAVNO + 15%]
from
    invoices
order by
    Total desc;
-- ^ Выполнено
-- Практическо задание
select
    InvoiceDate,
    BillingAddress,
    BillingCity,
    Total
from invoices
where
    Total between  0 and 1.98 or 1.98 and 5
order by
    InvoiceDate;
-- ^ Выполнено
select
    InvoiceDate,
    BillingAddress,
    BillingCity,
    Total
from
    invoices
where
    total IN (1.98,3.96)
order by
    invoiceDate;

select
    InvoiceDate,
    BillingAddress,
    BillingCity,
    Total
from
    invoices
where
        total IN (13.86,18.86,21.86)
order by
    invoiceDate;


select
    InvoiceDate,
    BillingAddress,
    BillingCity,
    Total
from
    invoices
where
    BillingCity not LIKE '%T%'
order by
    Total;


select
    InvoiceDate,
    BillingAddress,
    BillingCity,
    Total
from
    invoices
where
       InvoiceDate  between '2009-01-01' and '2009-12-31'
order by
    Total ;

select
    InvoiceDate,
    BillingAddress,
    BillingCity,
    Total
from
    invoices
where
    DATE (InvoiceDate) = '2009-01-03'
order by
    Total;


select
    InvoiceDate,
    BillingAddress,
    BillingCity,
    Total
from
    invoices
where
        DATE (InvoiceDate) > '2010-01-02' and Total <3
order by
    Total;


select
    InvoiceDate,
    BillingAddress,
    BillingCity,
    Total
from
    invoices
where
    BillingCity like 'p%' and Total>2.00

    order by
    Total;


select
    InvoiceDate,
    BillingAddress,
    BillingCity,
    Total
from
    invoices
where
    BillingCity like 'p%' or BillingCity like 'd%'

order by
    Total;

select
    InvoiceDate,
    BillingAddress,
    BillingCity,
    Total
from
    invoices
where
    Total > 1.98 and BillingCity like 'p%' or
    BillingCity like 'd%'
order by
    Total;


select
    InvoiceDate,
    BillingAddress,
    BillingCity,
    Total
from
    invoices
where
            Total > 1.98 and (BillingCity like 'p%' or
        BillingCity like 'd%')
order by
    Total;


select
    InvoiceDate,
    BillingAddress,
    BillingCity,
    Total
from
    invoices
where
        Total > 3.00 and (BillingCity like 'p%' or
        BillingCity like 'd%')
order by
    Total;

select
    InvoiceDate,
    BillingAddress,
    BillingCity,
    Total,
    case
        when Total < 2.00 then 'Baseline Purhase'
        when total between 2.00 and 6.99 then
        'Low Purchase'
        when Total between 7.00 and 15.00 then
        'Target Purchase'
        else 'Top Performers'
        end as PurchaseType
from
    invoices
where PurchaseType = 'Top Performers'
order by
    BillingCity;

/*
 Контрольная работа
 */
select
    InvoiceDate,
    BillingAddress,
    BillingCountry,
    BillingCity,
    Total,
    case
        when BillingCountry = 'USA'  then 'Domestic Sales'
        else 'Foreign Sales'
        end as SalesType
from
    invoices
where SalesType = 'Domestic Sales' and Total >15
order by
    BillingCountry;
-- выполнена

select
    *
from
    invoices
inner join customers
on invoices.CustomerId = customers.CustomerId;

select
    c.LastName,
    c.FirstName,
    i.InvoiceId,
    i.CustomerId,
    i.InvoiceDate,
    i.Total
from
    invoices as i
inner join
    customers as c
on i.CustomerId = c.CustomerId
order by
    c.LastName;


select
    i.InvoiceId,
    c.CustomerId,
    c.FirstName,
    c.Address,
    i.InvoiceDate,
    i.BillingAddress,
    i.Total
from
    invoices as i
left outer join customers as c
on i.CustomerId = c.CustomerId;


select
    e.FirstName,
    e.LastName,
    e.EmployeeId,
    c.FirstName,
    c.LastName,
    c.SupportRepId,
    i.CustomerId,
    i.Total
from
    invoices as i
inner join
        customers as c
on i.CustomerId = c.CustomerId
inner join employees as e
on c.SupportRepId = e.EmployeeId
order by
    i.Total desc
limit 10;


SELECT
    ar.ArtistId AS [ArtistId From Artists Table],
    al.ArtistId AS [ArtistId From Albums Table],
    ar.Name AS [Artist Name],
    al.Title AS [Album Title]
FROM artists AS ar
    LEFT OUTER JOIN albums AS al
    ON    ar.ArtistId = al.ArtistId
where
    al.ArtistId is null;

select
    t.TrackId,
    t.Composer,
    t.Name,
    al.AlbumId,
    al.Title
from
    tracks as t
left join albums as al
on t.AlbumId = al.AlbumId
where t.Composer is null;

--Контрольная работа
select
    t.TrackId,
    al.AlbumId,
    al.Title,
    g.Name
from
    tracks as t
    inner join albums as al
on t.AlbumId = al.AlbumId
    inner join genres as g
on t.GenreId = g.GenreId
--Выполнена







